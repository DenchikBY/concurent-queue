package by.denchik.threadedparser

import java.util.concurrent.Executors
import java.util.concurrent.LinkedTransferQueue

class Application {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val list = LinkedTransferQueue<String>()

            val pool = Executors.newWorkStealingPool()
            (1..5).forEach {
                pool.execute(ParserThread(list))
            }

            while (true) {
                print("Text: ")
                val str = readLine().toString()
                if (str.isEmpty()) continue
                list.add(str)
            }
        }
    }
}
