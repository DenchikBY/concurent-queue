package by.denchik.threadedparser

import java.util.concurrent.LinkedTransferQueue

class ParserThread(private val list: LinkedTransferQueue<String>) : Thread() {

    override fun run() {
        while (true) {
            if (list.isNotEmpty()) {
                Thread.sleep(1000)
                val str = list.poll()
                if (str === null) continue
                println(Thread.currentThread().name + ": " + str)
            } else {
                Thread.sleep(1000)
            }
        }
    }

    init {
        isDaemon = true
    }

}
